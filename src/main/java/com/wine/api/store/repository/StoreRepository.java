package com.wine.api.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wine.api.store.entity.StoreEntity;

@Repository
public interface StoreRepository extends JpaRepository<StoreEntity, String> {

}
