package com.wine.api.store.business;

import java.util.List;

import org.springframework.stereotype.Component;

import com.wine.api.store.entity.StoreEntity;
import com.wine.api.store.entity.dto.StoreDTO;

@Component
public interface StoreBusiness {

	public StoreEntity save(StoreDTO dto);
	
	public List<StoreEntity> findAll();
	
	public StoreEntity findById(String id);
	
	public StoreEntity findByZipCode(String zipCode);
	
	public void delete(String id);
	
	public void update();
	
	public void checkFields(StoreDTO dto);
	
}
