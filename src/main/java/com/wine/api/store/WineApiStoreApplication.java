package com.wine.api.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class WineApiStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(WineApiStoreApplication.class, args);
	}

}
